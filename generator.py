import cv2
import click
import numpy as np
from PIL import Image
from ISR.models import RRDN
from tqdm import tqdm

import os


def load_model():
	rrdn = RRDN(arch_params={'C':4, 'D':3, 'G':32, 'G0':32, 'T':10, 'x':4})
	rrdn.model.load_weights('weights/sample_weights/rrdn-C4-D3-G32-G032-T10-x4-GANS/rrdn-C4-D3-G32-G032-T10-x4_epoch299.hdf5')
	return rrdn

@click.command()
@click.option('--input_file', required=True, type=click.Path(exists=True, readable=True),
              help='Path to input video')
@click.option('--output_file',  default='./output.avi', type=click.Path(writable=True),
              help='Path and name of output file.')
def main(input_file, output_file):
	# load model
	model = load_model()

	video_read = cv2.VideoCapture(input_file)
	width  = video_read.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
	height = video_read.get(cv2.CAP_PROP_FRAME_HEIGHT) # float
	fps = video_read.get(cv2.CAP_PROP_FPS)
	fourcc = cv2.VideoWriter_fourcc(*'mp4v') 
	video_write = cv2.VideoWriter(output_file, fourcc, fps,(int(width)*4,int(height)*4)) # change resolution based on model
	success,image = video_read.read()

	print(f"Reading input file {input_file}")

	for i in tqdm(range(int(video_read.get(cv2.CAP_PROP_FRAME_COUNT)))):
		# transform image with rrdn
		upscaled_img = model.predict(image)
		# save img
		video_write.write(upscaled_img)
		success, image = video_read.read()
		if not success:
			break



if __name__ == '__main__':
    main()
