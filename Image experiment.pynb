{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Using TensorFlow backend.\n"
     ]
    }
   ],
   "source": [
    "from ISR.models import RDN, RRDN\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from PIL import Image\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "WARNING:tensorflow:From /home/kartol/miniconda3/envs/tfg/lib/python3.7/site-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.\n",
      "Instructions for updating:\n",
      "Colocations handled automatically by placer.\n"
     ]
    }
   ],
   "source": [
    "rrdn = RRDN(arch_params={'C':4, 'D':3, 'G':32, 'G0':32, 'T':10, 'x':4})\n",
    "rrdn.model.load_weights('weights/sample_weights/rrdn-C4-D3-G32-G032-T10-x4-GANS/rrdn-C4-D3-G32-G032-T10-x4_epoch299.hdf5')\n",
    "rdn = RDN(arch_params={'C':6, 'D':20, 'G':64, 'G0':64, 'x':2})\n",
    "rdn.model.load_weights('weights/sample_weights/rdn-C6-D20-G64-G064-x2/ArtefactCancelling/rdn-C6-D20-G64-G064-x2_ArtefactCancelling_epoch219.hdf5')\n",
    "\n",
    "class Img:\n",
    "    def __init__(self, img, crop=[1080, 500, 1360, 960]):\n",
    "        self.img = img\n",
    "        self.crop = crop\n",
    "    \n",
    "    def downscale(self, scale=2):\n",
    "        new_img = self.img.copy() # copy image inside of the object\n",
    "        new_img.thumbnail(tuple(size//scale for size in self.size), Image.ANTIALIAS) # downscale the new image\n",
    "        crop = [size//scale for size in self.crop] # get new crop\n",
    "        return Img(new_img, crop)\n",
    "    \n",
    "    def upscale(self, method, original_crop=None, original_size=(1920,1080)):\n",
    "        if method=='bilinear':\n",
    "            new_img = self.img.resize(tuple(size*2 for size in self.size),Image.BILINEAR)\n",
    "            crop = [size*2 for size in self.crop]\n",
    "        elif method=='rdn':\n",
    "            rdn_img = rdn.predict(self.array)\n",
    "            new_img = Image.fromarray(rdn_img)\n",
    "            crop = [size*2 for size in self.crop]\n",
    "        elif method=='rrdn':\n",
    "            rrdn_img = rrdn.predict(self.array)\n",
    "            new_img = Image.fromarray(rrdn_img)\n",
    "            crop = [size*4 for size in self.crop]\n",
    "        elif method=='bilinear to original size':\n",
    "            new_img = self.img.resize(original_size,Image.BILINEAR)\n",
    "            crop = original_crop\n",
    "        elif method=='rdn to original size':\n",
    "            rdn_img = rdn.predict(self.array)\n",
    "            new_img = Image.fromarray(rdn_img)\n",
    "            while new_img.size[0] < original_size[0]:\n",
    "                rdn_img = rdn.predict(rdn_img)\n",
    "                new_img = Image.fromarray(rdn_img)\n",
    "            new_img.thumbnail(original_size, Image.ANTIALIAS)\n",
    "            crop = original_crop\n",
    "        elif method=='rrdn to original size':\n",
    "            rrdn_img = rrdn.predict(self.array)\n",
    "            new_img = Image.fromarray(rrdn_img)\n",
    "            while new_img.size[0] < original_size[0]:\n",
    "                rrdn_img = rrdn.predict(rrdn_img)\n",
    "                new_img = Image.fromarray(rrdn_img)\n",
    "            new_img.thumbnail(original_size, Image.ANTIALIAS)\n",
    "            crop = original_crop\n",
    "        else:\n",
    "            raise Exception(\"Method of upscaling not recognized\")\n",
    "        \n",
    "        return Img(new_img, crop) \n",
    "\n",
    "    \n",
    "    def get_cropped_image(self):\n",
    "        return self.img.crop(self.crop)\n",
    "    \n",
    "    @property\n",
    "    def array(self):\n",
    "        return np.array(self.img)\n",
    "        \n",
    "    @property\n",
    "    def size(self):\n",
    "        return self.img.size\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def image_plotter(axs):\n",
    "    axs = axs.flat\n",
    "    def plot_image(X, title, **kwargs):\n",
    "        ax = next(axs) \n",
    "        ax.imshow(X,**kwargs)\n",
    "        ax.set_title(title)\n",
    "    return plot_image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_data(img_name, crop, scale_down_factors, methods):\n",
    "    \n",
    "    images = []\n",
    "    \n",
    "    for scale in scale_down_factors:\n",
    "        img = Image.open(img_name)\n",
    "        img.thumbnail((1920,1080), Image.ANTIALIAS) # make sure img is full hd. This will upscale it if few pixels are missing\n",
    "        images.append(Img(img,crop).downscale(scale))\n",
    "\n",
    "    \n",
    "    data = {'orig':images, 'bilinear':[], 'rdn':[], 'rrdn':[],\n",
    "            'bilinear to original size':[], 'rdn to original size':[], 'rrdn to original size':[]}\n",
    "    for image in images[1:]:\n",
    "        for method in methods:\n",
    "            data[method].append(image.upscale(method, crop))\n",
    "    return data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_data(img_name, crop, scale_down_factors, methods):\n",
    "    \n",
    "    fig = plt.Figure()\n",
    "    DPI = fig.get_dpi()\n",
    "    x_size_in_inches = 280/float(DPI)\n",
    "    y_size_in_inches = 460/float(DPI)\n",
    "\n",
    "    nrows=(len(scale_down_factors)-1)*2\n",
    "    ncols=5\n",
    "    \n",
    "    fig, axs = plt.subplots(nrows=nrows, ncols=ncols,\n",
    "                            subplot_kw={'xticks': [], 'yticks': []})\n",
    "\n",
    "    fig.set_size_inches(x_size_in_inches*ncols,y_size_in_inches*nrows)\n",
    "\n",
    "    plotter = image_plotter(axs)\n",
    "    for i, scale in enumerate(scale_down_factors[1:]):\n",
    "        # orig image\n",
    "        img = Image.open(img_name)\n",
    "        img = img.resize((1920,1080),Image.BILINEAR) # make sure img is full hd. This will upscale it if few pixels\n",
    "        ground_truth = Img(img,crop)\n",
    "        low_res = ground_truth.downscale(scale)\n",
    "        \n",
    "        plotter(low_res.get_cropped_image(),'low resolution downscale: '+str(scale) + \"x\")\n",
    "        for method in methods[:3]:\n",
    "            plotter(low_res.upscale(method).get_cropped_image(),'upscaled image with one iteration of '+ method)\n",
    "        plotter(ground_truth.get_cropped_image(),'original')\n",
    "        \n",
    "        plotter(low_res.get_cropped_image(),'low resolution downscale: '+str(scale) + \"x\")\n",
    "        for method in methods[3:]:\n",
    "            plotter(low_res.upscale(method,crop).get_cropped_image(),'upscaled image with '+ method)\n",
    "        plotter(ground_truth.get_cropped_image(),'original')\n",
    "\n",
    "    plt.tight_layout()\n",
    "    plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "scale_down_factors = [1, 2, 4, 8, 16, 32]\n",
    "methods = ['bilinear', 'rdn', 'rrdn','bilinear to original size','rdn to original size','rrdn to original size']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plot_data('BigBuckBunny_1080.png', [550, 0, 830, 460], scale_down_factors, methods)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plot_data('FoxBird_1080.png', [780, 250, 1060, 710], scale_down_factors, methods)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
