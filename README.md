# Video Resolution Upscaling

* Vezměte krátké 5-10 sekundové video a vytvořte generátor, který bude generovat video s vyšším rozlišením.
* Používejte obě architektury: GAN a U-Net.
* Porovnejte výsledky.

## State-of-the-art as of in 11/2019


### Video upscaling
The goal is to make a upscaled animated video.
There is a successful [paper](https://xinntao.github.io/projects/EDVR) about video resolution which uses multiple frames when upscaling a video.
This approach makes sense but is quite difficult and I will not use it, instead I will be trying to upsacle a video
the old way. Frame by frame.

### Image upscaling
There is a plenty of new papers about this topic for instance [Second-Order Attention Network for Single Image Super-Resolution](http://openaccess.thecvf.com/content_CVPR_2019/html/Dai_Second-Order_Attention_Network_for_Single_Image_Super-Resolution_CVPR_2019_paper.html) or [RankSRGAN](https://arxiv.org/abs/1908.06382).
I am going to try GAN version of image upscaling called [ESRGAN](https://arxiv.org/abs/1809.00219) and [Residual Dense Network for Image Super-Resolution](https://arxiv.org/abs/1802.08797). I will make use of a github project [idealo/image-super-resolution](https://github.com/idealo/image-super-resolution).



## Installation

There are few dependencies which can be installed with pip `pip install tqdm click opencv-python notebook numpy ISR matplotlib Pillow`

The ISR package needs to have weights of predefinied neural networks. Up to date install guide should be on this [link](https://idealo.github.io/image-super-resolution/#installation).
When I was installing it there was a problem with git lfs so instead of the guide in above link I just download the weights from [google drive](https://drive.google.com/drive/folders/1cJLPgGfEuFAQzBKbXQtSGXxLXssw1D9f). Then place folders with weights into folders `weights/sample_weights/`.
So create a directory for weights `mkdir -p weights/sample_weights/` and place downloaded weights into sample_weights directory.

#### Experiments
* [Image experiment](Image experiment.ipynb) can be run if previous were met. All the data needed is stored in root directory. 
* For [video experiment](Video experiment.ipynb) you have to obtain data. So download videos FoxBird_95_1080_5800.yuv a BigBuckBunny_95_1080_5800.yuv from [Netflix dataset](https://github.com/Netflix/vmaf/blob/master/resource/doc/datasets.md). Place it in data/ folder. Create folders for images `mkdir data/BigBuckBunny data/FoxBird` and run ffmpeg commands from `data/clips.txt` file. After having a data you should create folders for results `mkdir data/BigBuckBunny_16 data/BigBuckBunny_rrdn_16 data/BigBuckBunny_rdn_16 data/BigBuckBunny_bilinear_16 data/BigBuckBunny_nearest_16 data/FoxBird_16 data/FoxBird_rrdn_16 data/FoxBird_rdn_16 data/FoxBird_bilinear_16 data/FoxBird_nearest_16`. After running the IPython notebook those folders will contain 4 times upscaled images from 16 times downscaled original images. Video can be obtained with ffmpeg or with some video editor software. 

#### Generator
[Generator](generator.py) uses rrdn network from ISR project. You need to pass a video file as --input_file argument and then program creates `output.avi` with 4 times higher resolution. Example: `python generator.py --input_file data/video.avi` 